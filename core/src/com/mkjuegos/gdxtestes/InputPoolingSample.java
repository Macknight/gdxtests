package com.mkjuegos.gdxtestes;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mkjuegos.gdxtestes.utils.GdxUtils;

public class InputPoolingSample implements ApplicationListener,InputProcessor{

	private OrthographicCamera camera;
	private Viewport viewport;
	private SpriteBatch batch;
	private BitmapFont font;

	private static final Logger log = new Logger (InputListeningSample.class.getName(), Logger.DEBUG);
	private static final int MAX_MESSAGES_COUNT = 15;
	private final Array<String> messages= new Array<String>();

	@Override
	public void create() {
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		camera = new OrthographicCamera();
		viewport = new FitViewport(1080, 720,camera);
		Gdx.input.setInputProcessor(this);
		batch = new SpriteBatch();
		font  = new BitmapFont(Gdx.files.internal("fonts/oswald-32.fnt"));

	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width,height,true);
	}

	@Override
	public void render() {
		GdxUtils.clearScreen();

		batch.setProjectionMatrix(camera.combined);

		batch.begin();
		draw();
		batch.end();
	}

	private void draw() {
		for (int i = 0; i <messages.size; i++) {
			font.draw(batch, messages.get(i),20.0f, 720 - 40.0f * (i + 1));
		}


	}

	private void addMessage(String message){
		messages.add(message);

		if(messages.size > MAX_MESSAGES_COUNT){
			messages.removeIndex(0);
		}

	}


	@Override
	public void pause() {

	}


	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		batch.dispose();
		font.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		String message = "keyDown kkeycode= " + keycode;
		log.debug(message);
		addMessage(message);
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		String message = "touchDown screenX= " + screenX +  " screenY=" + screenY;
		log.debug(message);
		addMessage(message);
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		String message = "touchUp screenX= " + screenX +  " screenY=" + screenY;
		log.debug(message);
		addMessage(message);
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		String message = "touchDraged screenX= " + screenX +  " screenY=" + screenY;
		log.debug(message);
		addMessage(message);
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		String message = "mouseMoved screenX= " + screenX +  " screenY=" + screenY;
		log.debug(message);
		addMessage(message);
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		String message = "scrolled amount = " + amount;
		log.debug(message);
		addMessage(message);
		return false;
	}
}
