package com.mkjuegos.gdxtestes;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Logger;

public class ApplicationListenerSample implements ApplicationListener{

	private static final Logger log  = new Logger(ApplicationListenerSample.class.getName(),Logger.DEBUG);

	boolean showrender = true;
	@Override
	public void create() {
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		log.debug("create()");
	}

	@Override
	public void resize(int width, int height) {
		log.debug("resize()");
	}

	@Override
	public void render() {
		if(showrender) {
			log.debug("render()");
			showrender=false;
		}
	}

	@Override
	public void pause() {
		log.debug("pause");
	}

	@Override
	public void resume() {
		log.debug("resume()");
		showrender = true;
	}

	@Override
	public void dispose() {
		log.debug("despose()");
	}
}
