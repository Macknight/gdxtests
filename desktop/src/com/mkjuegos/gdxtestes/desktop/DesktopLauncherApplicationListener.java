package com.mkjuegos.gdxtestes.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mkjuegos.gdxtestes.ApplicationListenerSample;
import com.mkjuegos.gdxtestes.gdxTestes;

public class DesktopLauncherApplicationListener {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new ApplicationListenerSample(), config);
	}
}
