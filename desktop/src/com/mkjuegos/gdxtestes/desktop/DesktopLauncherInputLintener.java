package com.mkjuegos.gdxtestes.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mkjuegos.gdxtestes.InputListeningSample;
import com.mkjuegos.gdxtestes.InputPoolingSample;

public class DesktopLauncherInputLintener {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();


		new LwjglApplication(new InputListeningSample(), config);
	}
}
