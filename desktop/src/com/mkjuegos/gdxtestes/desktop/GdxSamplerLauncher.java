package com.mkjuegos.gdxtestes.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglAWTCanvas;


import javax.swing.*;
import java.awt.*;

public class GdxSamplerLauncher  extends JFrame{

    private static final int WIDTH = 1280;
    private static final int HEIGHT = 720;

    private LwjglAWTCanvas lwjglAWTCanvas;

    public GdxSamplerLauncher() throws HeadlessException {
       setTitle(GdxSamplerLauncher.class.getSimpleName());
       setMinimumSize(new Dimension(WIDTH, HEIGHT));
       setSize(WIDTH, HEIGHT);
       setResizable(false);
       setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        pack();
        setVisible(true);

    }


    public static void main(String[] args) {
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GdxSamplerLauncher();
            }
        });
    }
}
